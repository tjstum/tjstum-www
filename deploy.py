#!/usr/bin/env python3
"""
Deploy website files for tjstum.com

Actions:
  collect - Collect and print files to be uploaded
  s3-staging - Upload files to the staging site
  prod - Upload files to production site, refresh distribution
  preview - Start a local web server

All actions will render template files

--dryrun prevents most actual actions

"""
from __future__ import annotations

import functools
import sys
from argparse import ArgumentParser, Namespace, RawDescriptionHelpFormatter
from collections.abc import Callable, Collection, Iterator, Iterable
from typing import NoReturn, TypedDict, TYPE_CHECKING

import boto3
import yaml

if TYPE_CHECKING:
    from mypy_boto3_s3.service_resource import Bucket


class S3Config(TypedDict, total=False):
    bucket: str
    distribution: str


class S3Buckets(TypedDict):
    staging: S3Config
    production: S3Config


class FileConfig(TypedDict):
    core: list[str]
    compiled: dict[str, str]
    optional: dict[str, list[str]]


class YAMLConfig(TypedDict):
    files: FileConfig
    s3: S3Buckets


Action = Callable[[Namespace, YAMLConfig], None]

actions: dict[str, Action] = {}


def action(name: str) -> Callable[[Action], Action]:
    def decorator(func: Action) -> Action:
        actions[name] = func
        return func

    return decorator


def fail(msg: str) -> NoReturn:
    sys.exit(msg)


warn = functools.partial(print, file=sys.stderr)


session = boto3.Session(profile_name="web")


def upload_files(
    bucket: Bucket, file_paths: Collection[str], dryrun: bool
) -> set[str] | None:
    import os
    import mimetypes
    from datetime import datetime, timezone

    existing = bucket.objects.all()
    required = {
        name: datetime.fromtimestamp(os.stat(name).st_mtime, timezone.utc)
        for name in file_paths
    }
    to_delete = set()
    to_upload = set()
    for obj in existing:
        try:
            if required[obj.key] > obj.last_modified:
                to_upload.add(obj.key)
        except KeyError:
            to_delete.add(obj.key)
    to_upload |= required.keys() - {obj.key for obj in existing}
    print("Updating:", to_upload or "None", sep="\n")
    print("Deleting:", to_delete or "None", sep="\n")
    if dryrun:
        return None

    for key in to_upload:
        with open(key, "rb") as fd:
            mtype, encoding = mimetypes.guess_type(key)
            mtype = mtype or "text/plain"
            bucket.put_object(
                ACL="public-read",
                Body=fd,
                Key=key,
                ContentType=mtype,
                Metadata={"cache-control": "max-age=1800"},
            )
    if to_delete:
        bucket.delete_objects(Delete={"Objects": [{"Key": key} for key in to_delete]})
    return to_upload


def expand(paths: Iterable[str]) -> Iterator[str]:
    for path in paths:
        if "*" in path:
            import glob

            yield from glob.iglob(path)
        else:
            yield path


def collect_files(args: Namespace, config: YAMLConfig) -> list[str]:
    import os

    files = []
    for path in expand(config["files"]["core"]):
        if not os.path.exists(path):
            fail(f"Missing required file {path}")
        files.append(path)
    for collection, items in config["files"]["optional"].items():
        if not getattr(args, collection):
            warn("Skipping optional collection", collection)
            continue
        for path in expand(items):
            if not os.path.exists(path):
                fail(f"Missing path {path} which is required by {collection}")
            files.append(path)
    for template_name, dest_name in config["files"]["compiled"].items():
        process_template(template_name, dest_name, files, args.dryrun)
        if os.path.exists(dest_name):
            files.append(dest_name)
        else:
            warn(dest_name, "was not compiled")
    return files


def process_template(
    template_name: str, dest_name: str, file_paths: Iterable[str], dryrun: bool
) -> None:
    import os

    if template_name == "script.js.tmpl":
        image_paths = {
            path
            for path in file_paths
            if os.path.dirname(path) == "image" and path != "image/sentinel.png"
        }
        return compile_images(template_name, dest_name, image_paths, dryrun)
    raise ValueError(f"Unknown template {template_name!r}")


def compile_images(
    template_name: str, dest_name: str, images: Iterable[str], dryrun: bool
) -> None:
    import os
    from jinja2 import Environment, FileSystemLoader

    env = Environment(loader=FileSystemLoader("."))
    image_bases = [os.path.basename(path) for path in images]
    template = env.get_template(template_name)
    rendered = template.render(image_paths=image_bases)
    if not dryrun:
        with open(dest_name, "w") as fd:
            fd.write(rendered)


@action("s3-staging")
def s3upload(args: Namespace, config: YAMLConfig) -> None:
    bucket: Bucket = session.resource("s3").Bucket(config["s3"]["staging"]["bucket"])
    upload_files(bucket, collect_files(args, config), args.dryrun)
    print("http://tjstum-website-staging.s3-website-us-east-1.amazonaws.com/")


@action("prod")
def full_deploy(args: Namespace, config: YAMLConfig) -> None:
    import time

    bucket = session.resource("s3").Bucket(config["s3"]["production"]["bucket"])
    upload_files(bucket, collect_files(args, config), args.dryrun)
    # Nuking all of the files isn't optimal, but there are so few, it doesn't really matter
    cloud = session.client("cloudfront")
    cloud.create_invalidation(
        DistributionId=config["s3"]["production"]["distribution"],
        InvalidationBatch={
            "Paths": {"Quantity": 1, "Items": ["/*"]},
            "CallerReference": str(time.time()),
        },
    )


@action("collect")
def collect_only(args: Namespace, config: YAMLConfig) -> None:
    for path in collect_files(args, config):
        print(path)


@action("preview")
def web_server(args: Namespace, config: YAMLConfig) -> None:
    import http.server, socketserver

    with socketserver.TCPServer(
        ("127.0.0.1", 8080), http.server.SimpleHTTPRequestHandler
    ) as httpd:
        print("Running on http://127.0.0.1:8080/")
        httpd.serve_forever()


def main() -> None:
    parser = ArgumentParser(
        description=__doc__, formatter_class=RawDescriptionHelpFormatter
    )
    parser.add_argument("action", help="What to do", choices=actions.keys())
    parser.add_argument(
        "--dryrun", action="store_true", help="List operations but don't run them"
    )
    extras = parser.add_argument_group(
        "Extra files", "Control whether other files should be included"
    )
    try:
        with open("deploy.yml") as fd:
            config: YAMLConfig | None = yaml.safe_load(fd)
    except OSError as ex:
        # don't exit immediately so that --help works
        warn(f"Error opening config file: {ex!r}")
        config = None
    if config is not None:
        for collection in config["files"]["optional"].keys():
            extras.add_argument(
                f"--no-{collection}",
                dest=collection,
                action="store_false",
                help=f"Don't collect or upload from {collection}",
            )
    args = parser.parse_args()
    if config is None:
        fail("Can't continue without a config file")
    actions[args.action](args, config)


if __name__ == "__main__":
    main()
